package br.com.ucsal.model;

import javax.persistence.AttributeConverter;

public class SituacaoAlunoConverter implements AttributeConverter<SituacaoAlunoEnum, String> {

	public String convertToDatabaseColumn(SituacaoAlunoEnum attribute) {
		return attribute == null ? null : attribute.getCodigo();
	}

	public SituacaoAlunoEnum convertToEntityAttribute(String dbData) {
		return dbData == null ? null : SituacaoAlunoEnum.valueOfCodigo(dbData);
	}

}

