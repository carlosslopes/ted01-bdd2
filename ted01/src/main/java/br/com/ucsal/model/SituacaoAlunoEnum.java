package br.com.ucsal.model;

public enum SituacaoAlunoEnum {

	TRANCADO("TRC"), ATIVO("ATV");

	private String codigo;

	private SituacaoAlunoEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public static SituacaoAlunoEnum valueOfCodigo(String codigo) {
		for (SituacaoAlunoEnum situacao : values()) {
			if (situacao.getCodigo().equalsIgnoreCase(codigo)) {
				return situacao;
			}
		}
		throw new IllegalArgumentException("C�digo n�o encontrado: " + codigo);
	}

}
