package br.com.ucsal.model;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class Main {

	public static void main(String[] args) {
		AlunoDAO alunoDAO = new AlunoDAO();

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("academia");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		Aluno alunoAntonio = new Aluno("Antonio", SituacaoAlunoEnum.ATIVO);
		alunoAntonio.setEndereco(new Endereco("Rua 1", "123", "Paralela"));
		em.persist(alunoAntonio);

		Aluno alunoJoao = new Aluno("Joao Carlos", SituacaoAlunoEnum.TRANCADO);
		alunoJoao.setEndereco(new Endereco("Rua 2", "456", "Lobato"));
		em.persist(alunoJoao);

		AlunoDiaria alunoMarcello = new AlunoDiaria("Marcello", SituacaoAlunoEnum.ATIVO, 30);
		alunoMarcello.setEndereco(new Endereco("Rua 3", "789", "Paripe"));
		em.persist(alunoMarcello);

		AlunoDiaria alunoCarlos = new AlunoDiaria("Carlos", SituacaoAlunoEnum.ATIVO, 30);
		alunoCarlos.setEndereco(new Endereco("Rua 4", "999", "Val�ria"));
		em.persist(alunoCarlos);

		em.getTransaction().commit();

		em.clear();

		Aluno alunoJoaoCarlos = em.merge(alunoJoao);
		em.persist(alunoJoaoCarlos);

		alunoAntonio = em.find(Aluno.class, alunoAntonio.getMatricula());
		alunoMarcello = em.find(AlunoDiaria.class, alunoMarcello.getMatricula());

		alunoCarlos = em.find(AlunoDiaria.class, alunoCarlos.getMatricula());
		em.remove(alunoCarlos);

		////////////////
		alunoDAO.setQuery("select a from Aluno a where a.idade >= :idade order by a.nome asc");
		alunoDAO.setIdade(18);
		TypedQuery<Aluno> queryIdade = em.createQuery(alunoDAO.getQuery(), Aluno.class);
		queryIdade.setParameter("idade", alunoDAO.getIdade());
		List<Aluno> alunosAdultos = queryIdade.getResultList();

		System.out.println("Alunos com 18 anos: ");
		alunosAdultos.stream().forEach(System.out::println);

		//////////////
		alunoDAO.setQuery("select a from Aluno a where a.peso >= :peso AND a.altura < :altura order by a.nome asc");
		alunoDAO.setPeso(120);
		alunoDAO.setAltura(170);

		TypedQuery<Aluno> queryObesidade = em.createQuery(alunoDAO.getQuery(), Aluno.class);
		queryObesidade.setParameter("peso", alunoDAO.getPeso());
		queryObesidade.setParameter("altura", alunoDAO.getAltura());
		List<Aluno> alunosObesos = queryObesidade.getResultList();

		System.out.println("Alunos com Obesidade: ");
		alunosObesos.stream().forEach(System.out::println);

		//////////////////
		alunoDAO.setQuery("select a from Aluno a where a.nome = :nome");
		alunoDAO.setNome("Carlos");

		TypedQuery<Aluno> queryNome = em.createQuery(alunoDAO.getQuery(), Aluno.class);
		queryNome.setParameter("nome", alunoDAO.getNome());
		List<Aluno> alunosIguais = queryNome.getResultList();

		System.out.println("Alunos com nomes iguais: ");
		alunosIguais.stream().forEach(System.out::println);

		em.close();
		emf.close();

	}

}
