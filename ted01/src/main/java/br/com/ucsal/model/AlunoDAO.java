package br.com.ucsal.model;

public class AlunoDAO {

	private Integer idade;
	private String nome;
	private Integer altura;
	private Integer peso;
	private String query;

	public AlunoDAO() {
		super();
	}

	public AlunoDAO(Integer idade, Integer altura, Integer peso, String query, String nome) {
		super();
		this.idade = idade;
		this.altura = altura;
		this.peso = peso;
		this.query = query;
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getAltura() {
		return altura;
	}

	public void setAltura(Integer altura) {
		this.altura = altura;
	}

	public Integer getPeso() {
		return peso;
	}

	public void setPeso(Integer peso) {
		this.peso = peso;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

}
