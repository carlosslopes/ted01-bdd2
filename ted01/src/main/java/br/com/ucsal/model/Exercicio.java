package br.com.ucsal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "exercicio")
public class Exercicio {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Integer id;

	@Column(name = "titulo")
	private String titulo;
	
	@Column(name = "descricao")
	private String descricao;

	@Column(name = "numeroDeSeries")
	private Integer numeroDeSeries;

	@Column(name = "numeroDeRepeticoes")
	private Integer numeroDeRespeticoes;
	
	@ManyToOne
    @JoinColumn(name = "treino_id")
    private Treino treino;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getNumeroDeSeries() {
		return numeroDeSeries;
	}

	public void setNumeroDeSeries(Integer numeroDeSeries) {
		this.numeroDeSeries = numeroDeSeries;
	}

	public Integer getNumeroDeRespeticoes() {
		return numeroDeRespeticoes;
	}

	public void setNumeroDeRespeticoes(Integer numeroDeRespeticoes) {
		this.numeroDeRespeticoes = numeroDeRespeticoes;
	}

	public Treino getTreino() {
		return treino;
	}

	public void setTreino(Treino treino) {
		this.treino = treino;
	}
	
	
}
