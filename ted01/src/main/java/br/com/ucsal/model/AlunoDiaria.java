package br.com.ucsal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "aluno_diaria")
public class AlunoDiaria extends Aluno {

	@Column(nullable = false)
	private Double valorDaDiaria;

	public AlunoDiaria() {
		super();
	}

	public AlunoDiaria(String nome, SituacaoAlunoEnum situacao, double valorDaDiaria) {
		super(nome, situacao);
		this.valorDaDiaria = valorDaDiaria;
	}

	public Double getValorDaDiaria() {
		return valorDaDiaria;
	}

	public void setValorDaDiaria(double valorDaDiaria) {
		this.valorDaDiaria = valorDaDiaria;
	}

	@Override
	public String toString() {
		return "AlunoDiaria [valorDaDiaria=" + valorDaDiaria + ", toString()=" + super.toString() + "]";
	}

}
