package br.com.ucsal.model;

import javax.persistence.Embeddable;

@Embeddable
public class Endereco {

	private String logradouro;

	private String complemento;

	private String bairro;

	public Endereco() {
	}

	public Endereco(String logradouro, String complemento, String bairro) {
		super();
		this.logradouro = logradouro;
		this.complemento = complemento;
		this.bairro = bairro;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

}
