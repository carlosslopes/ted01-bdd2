# TED01 BDD2 & GCM
##### Equipe BD:
Antonio Carlos,
Berman Schultz,
Luiz Felipe Viana
<br/>

##### Equipe GCM:
Antonio Carlos, 
Berman Schultz,
Luiz Felipe Viana,
Mauricio Mendes,
Yuri Melo
<br/><br/>


## ESCOPO (BDD2)
Disponibilidade de uma API com foco em fornecer um modelo de administração de sistemas de ginástica, musculação, academia e afins.
Com um escopo levando em base as entidades básicas como: Professor, Aluno, Treino, Disponibilidade, Horários,
Check-in/out, etc.
<br/><br/><br/>

## VISÃO DO PRODUTO (GCM)
Para alunos, profissionais da saúde (educação física, nutrição) e academias 

QUE buscam uma melhora no estilo de vida e praticidade na administração e consulta nas informações do sistema 

O GymClass 

É um aplicativo e sistema web de gerenciamento e estilo de vida 

QUE mantém e geri as informações das atividades e usuários melhorando sua experiência e auxiliando os mesmos a manter um lifestyle saudável 

DIFERENTE DO SmartFit, Nike Training 

NOSSO PRODUTO ajuda os alunos a terem mais informações das atividades, aumenta o nivel de comunicação entre aluno, professor e auxilia e permite uma melhor gestão e administração para as academias gerirem suas empresas.

 

